#
# ~/.bash_profile
#

export PATH="${PATH}:/home/red/scripts"
export EDITOR="vim"
export READER="zathura"
export FILE="ranger"
export BROWSER="qutebrowser"

[[ -f ~/.bashrc ]] && . ~/.bashrc

[ "$(tty)" = "/dev/tty1" ] && ! pgrep -x i3 > /dev/null && exec startx
