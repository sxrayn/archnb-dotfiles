#!/bin/bash

set -o vi
shopt -s autocd
HISTSIZE= HISTFILESIZE=
export PS1="\[$(tput setaf 6)\]\u\[$(tput setaf 14)\]@\[$(tput setaf 6)\]\h\[$(tput setaf 6)\]:\[$(tput setaf 4)\]\w \[$(tput setaf 14)\]> \[$(tput sgr0)\]"
setxkbmap -layout de,us -option grp:alt_shift_toggle


# wal integration
(cat ~/.cache/wal/sequences &)
#source ~/.cache/wal/colors-tty.sh


# Adding color
alias ls="ls -hN --color=auto --group-directories-first"
alias la="ls -AhN --color=auto --group-directories-first"
alias lx="ls -XhN --color=auto --group-directories-first"
alias ll="ls -lhN --color=auto --group-directories-first"
alias lla="ls -lAhN --color=auto --group-directories-first"
alias grep="grep --color=auto"
alias diff="diff --color=auto"
alias ccat="highlight --out-format=ansi" # Color cat - print file with syntax highlighting.

# Internet
alias yt="youtube-dl --add-metadata -i" # Download video link
alias yta="yt -x -f bestaudio/best" # Download only audio
alias YT="youtube-viewer"

# suspend
alias suspend="betterlockscreen -s dimblur"
alias lock="betterlockscreen -l dimblur"

# misc
alias scim="sc-im"

# systemctl
alias ss="sudo systemctl"
